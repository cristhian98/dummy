package com.journaldev.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.journaldev.spring.model.Person;
import com.journaldev.spring.service.PersonService;

@Controller
public class PersonController {

	private PersonService personService;

	//Es cuando la clase no esta mapeada en servlet-context.xml
	@Autowired(required=true)
	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}
	
	
	@RequestMapping(value="/inicio")
	public String inicio(Model model){
		
		//Es similiar al request en el servlet
		//request.setAtribute();
		model.addAttribute("person", new Person());
		
		//Segun el spring
		//person ----> /WEB-INF/views/person.jsp
		return "person"; // 
	}
	@RequestMapping(value="/registraPersona")
	public String registrarPersona(Person p){
		try {
			personService.registro(p);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "person";
	}
	
	public PersonService getPersonService() {
		return personService;
	}

	
	
	
	
}
