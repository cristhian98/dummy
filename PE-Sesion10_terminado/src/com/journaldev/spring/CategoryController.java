package com.journaldev.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.journaldev.spring.model.Category;
import com.journaldev.spring.model.Person;
import com.journaldev.spring.service.CategoryService;
import com.journaldev.spring.service.PersonService;

@Controller
public class CategoryController {

	private CategoryService categoryService;

	//Es cuando la clase no esta mapeada en servlet-context.xml
	@Autowired(required=true)
	
	@RequestMapping(value="/inicio")
	public String inicio(Model model){
		
		//Es similiar al request en el servlet
		//request.setAtribute();
		model.addAttribute("category", new Category());
		
		//Segun el spring
		//person ----> /WEB-INF/views/person.jsp
		return "category"; // 
	}
	public CategoryService getCategoryService() {
		return categoryService;
	}
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	@RequestMapping(value="/registraCategory")
	public String registrarCategory(Category c){
		try {
			categoryService.registro(c);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "category";
	}
	

}
