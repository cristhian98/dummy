package com.journaldev.spring.dao;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.journaldev.spring.model.Person;

@Repository
public class PersonDAOImpl implements PersonDAO{

	//Enviar mensajes en consola (Se pueden desactivar)
	private static Logger log = LoggerFactory.getLogger(PersonDAOImpl.class);
			
	//En persistencia session es conexion
	private SessionFactory sessionFactory;
	
	@Override
	public void registro(Person p) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.persist(p);
		log.info("Se registr� correctamente : " + p.getName());
	}

	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Autowired(required=true)
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	

}
