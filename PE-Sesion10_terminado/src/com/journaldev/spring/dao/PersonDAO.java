package com.journaldev.spring.dao;

import com.journaldev.spring.model.Person;

public interface PersonDAO {

	public void registro(Person p) throws Exception;
	
}
