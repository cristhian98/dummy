package com.journaldev.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.journaldev.spring.dao.PersonDAO;
import com.journaldev.spring.model.Person;

@Service
public class PersonServiceImpl implements PersonService {

	private PersonDAO personDAO;

	@Override
	@Transactional
	public void registro(Person p) throws Exception {
		personDAO.registro(p);
	}

	public PersonDAO getPersonDAO() {
		return personDAO;
	}
	
	@Autowired(required=true)
	public void setPersonDAO(PersonDAO personDAO) {
		this.personDAO = personDAO;
	}

}
