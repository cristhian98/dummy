package com.journaldev.spring.service;

import com.journaldev.spring.model.Category;
import com.journaldev.spring.model.Person;

public interface CategoryService {

	public void registro(Category c) throws Exception;
}
