package com.journaldev.spring.service;

import com.journaldev.spring.model.Person;

public interface PersonService {

	public void registro(Person p) throws Exception;
}
